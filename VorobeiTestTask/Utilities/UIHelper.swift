//
//  UIHelper.swift
//  VorobeiTestTask
//
//  Created by Илья Андреев on 14.04.2022.
//

import UIKit

enum UIHelper {
    
    static func createFiveColumnFlowLayout(in view: UIView) -> UICollectionViewFlowLayout {
        let width = view.bounds.width
        let padding: CGFloat = 12
        let minimumItemSpacing: CGFloat = 10
        let availableWidth = width - (padding * 2) - (minimumItemSpacing * 2)
        let itemWidth = availableWidth / 6
        
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.sectionInset = UIEdgeInsets(top: padding, left: padding, bottom: padding, right: padding)
        flowLayout.itemSize = CGSize(width: itemWidth, height: 5 * itemWidth)
        
        return flowLayout
    }
}

enum Images {
    static let shuffle: UIImage = .init(systemName: "shuffle")!
}
