//
//  GalleryCellModel.swift
//  VorobeiTestTask
//
//  Created by Илья Андреев on 14.04.2022.
//

import Foundation

struct GalleryCellModel: Hashable {
    let color: Int
    let id = UUID()
}
