//
//  CollectionViewController.swift
//  VorobeiTestTask
//
//  Created by Илья Андреев on 14.04.2022.
//

import UIKit

class CollectionViewController: UIViewController {
    
    enum Section: Hashable { case main }
    
    private var cells: [GalleryCellModel] = [
        .init(color: 0x00ffff),
        .init(color: 0x00ff00),
        .init(color: 0xff0000),
        .init(color: 0xc0c0c0),
        .init(color: 0xffff00)
    ]
    
    private var collectionView: UICollectionView!
    private var dataSource: UICollectionViewDiffableDataSource<Section, GalleryCellModel>!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureViewController()
        configureCollectionView()
        configureDataSource()
        update(on: cells)
    }
    
    private func configureViewController() {
        navigationItem.rightBarButtonItem = .init(
            image: Images.shuffle,
            style: .plain,
            target: self,
            action: #selector(shuffle)
        )
    }
    
    private func configureCollectionView() {
        collectionView = UICollectionView(
            frame: view.bounds,
            collectionViewLayout: UIHelper.createFiveColumnFlowLayout(in: view)
        )
        view.addSubview(collectionView)
        collectionView.backgroundColor = .systemBackground
        collectionView.register(
            UICollectionViewCell.self,
            forCellWithReuseIdentifier: UICollectionViewCell.description()
        )
        collectionView.backgroundColor = .systemBackground
        collectionView.dragDelegate = self
        collectionView.dropDelegate = self
        collectionView.dragInteractionEnabled = true
    }
    
    private func configureDataSource() {
        dataSource = UICollectionViewDiffableDataSource<Section, GalleryCellModel>(
            collectionView: collectionView
        ) { collectionView, indexPath, config in
            let cell = collectionView.dequeueReusableCell(
                withReuseIdentifier: UICollectionViewCell.description(), for: indexPath
            )
            cell.backgroundColor = UIColor(rgb: config.color)
            return cell
        }
    }
    
    private func update(on cells: [GalleryCellModel]) {
        var snapshot = NSDiffableDataSourceSnapshot<Section, GalleryCellModel>()
        snapshot.appendSections([.main])
        snapshot.appendItems(cells)
        dataSource.apply(snapshot, animatingDifferences: true)
    }
    
    @objc private func shuffle() {
        cells = cells.shuffled()
        update(on: cells)
    }
}

extension CollectionViewController: UICollectionViewDragDelegate {
    func collectionView(_ collectionView: UICollectionView, itemsForBeginning session: UIDragSession, at indexPath: IndexPath) -> [UIDragItem] {
        let cell = cells[indexPath.row]
        let dragItem = UIDragItem(itemProvider: NSItemProvider())
        dragItem.localObject = cell
        return [dragItem]
    }
}

extension CollectionViewController: UICollectionViewDropDelegate {
    func collectionView(_ collectionView: UICollectionView, performDropWith coordinator: UICollectionViewDropCoordinator) {
        guard let destinationIndexPath = coordinator.destinationIndexPath else { return }
        reorderItems(
            coordinator: coordinator,
            destinationIndexPath: destinationIndexPath,
            collectionView: collectionView
        )
    }
    
    func collectionView(_ collectionView: UICollectionView, dropSessionDidUpdate session: UIDropSession, withDestinationIndexPath destinationIndexPath: IndexPath?) -> UICollectionViewDropProposal {
        if collectionView.hasActiveDrag {
            return UICollectionViewDropProposal(operation: .move, intent: .insertAtDestinationIndexPath)
        } else {
            return UICollectionViewDropProposal(operation: .forbidden)
        }
    }
    
    private func reorderItems(coordinator: UICollectionViewDropCoordinator, destinationIndexPath: IndexPath, collectionView: UICollectionView) {
        if let item = coordinator.items.first,
           let sourceIndexPath = item.sourceIndexPath {
            coordinator.drop(item.dragItem, toItemAt: destinationIndexPath)
            cells.remove(at: sourceIndexPath.item)
            cells.insert(item.dragItem.localObject as! GalleryCellModel, at: destinationIndexPath.item)
            update(on: cells)
        }
    }
}
